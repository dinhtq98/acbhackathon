import React from 'react';

const Cards = React.lazy(() => import('./views/Base/Cards'));
const Dropdowns = React.lazy(() => import('./views/Base/Dropdowns'));
const Forms = React.lazy(() => import('./views/Base/Forms'));
const Tables = React.lazy(() => import('./views/Base/Tables'));
const Charts = React.lazy(() => import('./views/Charts'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Alerts = React.lazy(() => import('./views/Notifications/Alerts'));
const Badges = React.lazy(() => import('./views/Notifications/Badges'));
const Modals = React.lazy(() => import('./views/Notifications/Modals'));
const Targetform = React.lazy(() => import('./views/Base/Forms/Targetform'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

const States = React.lazy(() => import('./views/Pages/States'));
const CardDetails = React.lazy(() => import('./views/Pages/CardDetails'));
const Notifications = React.lazy(() => import('./views/Pages/Notifications'));

const addmember = React.lazy(() => import('./views/Base/Forms/Addmember'));
const addbill = React.lazy(() => import('./views/Base/Forms/Addbill'));
const Plan = React.lazy(() => import('./views/Pages/PlanList'));



// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Trang chủ' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/info', exact: true, name: 'Thông tin cá nhân', component: Cards },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/forms', name: 'Forms', component: Forms },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/plan', exact: true, name: 'Kế hoạch chi trả', component: Plan },
  { path: '/plan/alerts', name: 'Alerts', component: Alerts },
  { path: '/plan/badges', name: 'Badges', component: Badges },
  { path: '/plan/modals', name: 'Modals', component: Modals },
  { path: '/target', name: 'Mục tiêu', component: Targetform },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },

  { path: '/muctieu', name: 'Mục tiêu', component: States },
  { path: '/chitiet', name: 'Chi tiết', component: CardDetails },
  { path: '/thongbao', name: 'Thông báo', component: Notifications },

  { path: '/addmember', name: 'Thêm thành viên', component: addmember },
  { path: '/addbill', name: 'Thêm hóa đơn', component: addbill },

];

export default routes;

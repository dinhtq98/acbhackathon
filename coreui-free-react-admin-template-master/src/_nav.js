export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Kế hoạch chi trả',
      url: '/plan',
      icon: 'icon-bell',
      // children: [
      //   {
      //     name: 'Alerts',
      //     url: '/plan/alerts',
      //     icon: 'icon-bell',
      //   },
      //   {
      //     name: 'Badges',
      //     url: '/plan/badges',
      //     icon: 'icon-bell',
      //   },
      //   {
      //     name: 'Modals',
      //     url: '/plan/modals',
      //     icon: 'icon-bell',
      //   },
      // ],
    },
    {
      name: 'Mục tiêu',
      url: '/muctieu',
      icon: 'icon-calculator',
      // children: [
      //   {
      //     name: 'Thêm thành viên',
      //     url: '/target/addmember',
      //   },
      // ],
    },
  ],
};

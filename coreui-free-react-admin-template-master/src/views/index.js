import {
  Cards,
  Dropdowns,
  Forms,
  Tables,
} from './Base';

import Charts from './Charts';
import { Alerts, Badges, Modals } from './Notifications';
import { Login, Page404, Page500, Register } from './Pages';
import Widgets from './Widgets';

export {
  Badges,
  Page404,
  Page500,
  Register,
  Login,
  Modals,
  Alerts,
  Tables,
  Charts,
  Widgets,
  Forms,
  Dropdowns,
  Cards,
};


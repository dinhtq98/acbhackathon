import React, {Component} from 'react';
import {Button, Card, CardBody, CardHeader, Col, Row, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import classnames from 'classnames';
import Widget from '../Widgets/Widget05';
import Popup from "reactjs-popup";
class CardDetails extends Component {

  constructor(props) {
    super(props);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.state = {
      isOpen: false,
      activeTab: new Array(4).fill('1'),
      success: false,
      value: {
        targetName: 'Du lịch Nhật Bản',
        startDate: 'Ngày: 12/10/2019',
        // color: '',
        value: '75',
        saving: '15.000.000',
        target: '20.000.000',
        moneyPerMonth:'2.000.000',
        status:'Hoạt động',
      }
      
    };
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  
  
  render() {
    return (
      <div>
        <Widget value={this.state.value} />
        
      </div>
      
    );
  }
}

export default CardDetails;
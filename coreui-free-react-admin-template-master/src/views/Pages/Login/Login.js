import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { getToken } from '../../../services/auth.service';
import bgImage from '../../../assets/img/icon/OnBoardbg.png';
import './theme.css';
class Login extends Component {
  constructor (props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    }
  }


  signIn(e) {
    this.props.history.push('/');
  }



  handleChange = (event) =>{
    console.log(event.target.name + ':' + event.target.value)
    this.setState({[event.target.name]: event.target.value});
  }

  onSubmit = (e) => {
    e.preventDefault();
    const state = this.state;
    
    if (state.username && state.password) {

      fetch('http://192.168.137.1:8080/login', {

        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: state.username,
          password: state.password,
        }),
      })

      .then(res => res.text())
      .then(resJson => {
        if (resJson === 'VALID') {
          localStorage.setItem('username',this.state.username);
          this.signIn();
        } else {
          console.log('Hello');
        }
      })
      .catch(err => console.log(err));
    }

    return;
  }

  render() {
    return (
      <div className="container app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <Card className="p-2">
                <CardBody>
                  <Form onSubmit={this.onSubmit}>
                    <h1>Đăng nhập</h1>

                    <InputGroup className="mb-3" style={{marginTop:20}}>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input 
                        name="username"
                        type="text" 
                        placeholder="Username" 
                        autoComplete="username" 
                        required 
                        value={this.state.username} 
                        onChange={this.handleChange}/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="password"
                        type="password" 
                        placeholder="Password" 
                        autoComplete="current-password" 
                        required 
                        value={this.state.password}
                        onChange={this.handleChange}/>
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" className="px-4" type="submit">Đăng nhập</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
              
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
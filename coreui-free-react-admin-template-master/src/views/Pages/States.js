import React, {Component} from 'react';
import {Badge, Col, Nav, NavItem, NavLink, Row, TabContent, TabPane} from 'reactstrap';
import classnames from 'classnames';
import Widget from '../Widgets/Widget01';
import { Container, Button, lightColors, darkColors } from 'react-floating-action-button';
import { Redirect } from 'react-router-dom'

class States extends Component {

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      activeTab: new Array(4).fill('1'),
      value: [{
        targetName: 'Du lịch Nhật Bản',
        startDate: 'Ngày đạt: 12/10/2019',
        // color: '',
        value: '75',
        saving: '15.000.000',
        target: '20.000.000',
        color: 'primary',
        icon: 'travel'
      },
      {
        targetName: 'Du lịch Đà Lạt',
        startDate: 'Ngày đạt: 04/03/2019',
        // color: '',
        value: '100',
        saving: '5.000.000',
        target: '5.000.000',
        color: 'success',
        icon: 'travel'
      },
      {
        targetName: 'Mua xe ô tô',
        startDate: 'Ngày đạt: 04/05/2019',
        // color: '',
        value: '100',
        saving: '930.000.000',
        target: '930.000.000',
        color: 'success',
        icon: 'car'
      },
    ]
    };
  }

  getOngoing() {
    return (
        <Widget value={this.state.value[0]} 
        onClick={this.openDetail}
        />
      )
  }

  getWaiting() {
    return (
        <Widget value={this.state.value[0]} 
        onClick={this.openDetail}
        />
      )
  }

  getFinished() {
    return (
        <div>
          <Widget value={this.state.value[1]} 
        onClick={this.openDetail}
        />
          <Widget value={this.state.value[2]} 
        onClick={this.openDetail}
        />
        </div>
      )
  }

  // onGetDetail = () => {

  // }

  toggle(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({
      activeTab: newArray,
    });
  }

  tabPane() {
    return (
      <>
        <TabPane tabId="1">
          {this.getOngoing()}
        </TabPane>
        <TabPane tabId="2">
          {this.getWaiting()}
        </TabPane>
        <TabPane tabId="3">
          {this.getFinished()}
        </TabPane>
      </>
    );
  }

  onCreate = () => {
    this.props.history.push('/addmember')
  }
  openDetail = () => {
    this.props.history.push('/chitiet')

  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Nav tabs>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '1'}
                  onClick={() => { this.toggle(0, '1'); }}
                >
                  Hoạt động
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '2'}
                  onClick={() => { this.toggle(0, '2'); }}
                >
                  Tạm dừng
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  active={this.state.activeTab[0] === '3'}
                  onClick={() => { this.toggle(0, '3'); }}
                >
                  Hoàn thành
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab[0]}>
              {this.tabPane()}
            </TabContent>
          </Col>

        </Row>
        <Container>
          <Button
           styles={{color: lightColors.white}}
           className="bg-primary"
           onClick={this.onCreate}
          > Tạo
          </Button>
        </Container>
      </div>
      
    );
  }
}

export default States;
import React, {Component} from 'react';
import {Button, Card, CardBody, CardHeader, Col, Row, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import classnames from 'classnames';
import Widget from '../Widgets/Widget06';
// import {notification} from '../../constants/Mock.js';
const notification = [
    {
        userID : "BSQUOCHUY",
        title: "Mục tiêu hoạch định", 
        content: "Bạn đã chi tiêu 327,000 VND vượt mức chi tiêu trung bình hằng ngày là 271,000 VND. Bạn cần chi tiêu hợp lý hơn nha để đạt mục tiêu đã hoạch định.",
        date: "12/10/2019 21:58:23"
    },

    {
        userID : "BSQUOCHUY",
        title: "Mục tiêu hoạch định",
        content: "Số tiền mặt 1,000,000 VND bạn vừa rút nếu chi tiêu trong vòng 4 ngày thì bạn sẽ đạt được mục tiêu đã hoạch định nhé.",
        date: "12/10/2019 16:35:11"
    },

    {
        userID : "BSQUOCHUY",
        title: "Nhóm Gia đình nhỏ, Mục tiêu Trả tiền trọ hàng tháng",
        content: "Mục tiêu Trả tiền trọ hàng tháng của nhóm Gia đình nhỏ hiện tại đang bị trì hoãn do một số thành viên chưa hoàn thành mức đóng góp tích lũy của mình.",
        date: "12/10/2019 10:35:11"
    }
];

class Notifications extends Component {

  constructor(props) {
    super(props);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.state = {
      isOpen: false,
      activeTab: new Array(4).fill('1'),
      value: notification,
      
    };
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  
  
  render() {
    return (
      <div>
        {notification.map(data => {
          return (
            <Widget value={data} />
            )
        })}
        
      </div>
      
    );
  }
}

export default Notifications;
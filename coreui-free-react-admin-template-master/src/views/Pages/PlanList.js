import React, {Component} from 'react';
import { Card, CardBody, CardHeader, Col, Row, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import classnames from 'classnames';
import { Container, Button, lightColors, darkColors } from 'react-floating-action-button';
import Widget from '../Widgets/Widget06';

class PlanLists extends Component {

  constructor(props) {
    super(props);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.state = {
      isOpen: false,
      activeTab: new Array(4).fill('1'),
      value: {
        title: 'Internet',
        date: '30/09/2019',
        content:'Đây chính là mô tả',
      }
      
    };
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }

  onCreate = () => {
    this.props.history.push('/addbill')
  }
  openDetail = () => {
    this.props.history.push('/chitiet')
  }
  
  
  render() {
    return (
      <div>
        <Widget value={this.state.value} />
        <Container>
          <Button
           styles={{color: lightColors.white}}
           className="bg-primary"
           onClick={this.onCreate}
          > Tạo
          </Button>
        </Container>
      </div>
      
    );
  }
}

export default PlanLists;
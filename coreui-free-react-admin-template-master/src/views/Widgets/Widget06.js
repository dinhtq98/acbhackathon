import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Progress } from 'reactstrap';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';
import world from '../../assets/img/world.png';
import { Container, Row, Col } from 'reactstrap';
const propTypes = {
  header: PropTypes.string,
  mainText: PropTypes.string,
  smallText: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  variant: PropTypes.string,
};

const defaultProps = {
  targetName: '89.9%',
  mainText: 'Lorem ipsum...',
  smallText: 'Lorem ipsum dolor sit amet enim.',
  // color: '',
  value: '25',
  variant: '',
  saving: '15.000.000',
  target: '20.000.000',
};

class Widget06 extends Component {

  render() {
    const { className, cssModule, header, mainText, smallText, color, value, children, variant, ...attributes } = this.props;
  
    // demo purposes only
    const progress = { style: '', color: color, value: value.value };
    const card = { style: '', bgColor: 'bg-warning' };

    if (variant === 'inverse') {
      progress.style = 'progress-white';
      progress.color = '';
      card.style = 'text-white';
      card.bgColor = 'bg-' + color;
    }

    const classes = mapToCssModules(classNames(className, card.style, card.bgColor), cssModule);
    progress.style = classNames('progress-xs my-3', progress.style);

    return (
      <Card className={classes} {...attributes}>
        <CardBody>
          <Row style={{marginBottom:'15px'}}>
            <Col>
              <div className="h6 m-0">{value.title}</div>
            </Col>
          </Row>
          <Row style={{marginBottom:'15px'}}>
            <Col>
              <div>{value.content}</div>
            </Col>
          </Row>  
          <Row>
            <Col>

              <small>{value.date}</small>
            
            </Col>
          </Row>       

        </CardBody>
      </Card>
    );
  }
}

Widget06.propTypes = propTypes;

export default Widget06;

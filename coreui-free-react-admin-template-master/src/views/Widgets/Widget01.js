import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, Progress } from 'reactstrap';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';
import world from '../../assets/img/world.png';
import car from '../../assets/img/icon/car.png';

import { Container, Row, Col } from 'reactstrap';
const propTypes = {
  header: PropTypes.string,
  mainText: PropTypes.string,
  smallText: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  variant: PropTypes.string,
};

const defaultProps = {
  targetName: '89.9%',
  mainText: 'Lorem ipsum...',
  smallText: 'Lorem ipsum dolor sit amet enim.',
  // color: '',
  value: '25',
  variant: '',
  saving: '15.000.000',
  target: '20.000.000',
};

class Widget01 extends Component {

  render() {
    const { className, cssModule, header, mainText, smallText, color, value, children, variant, ...attributes } = this.props;
  
    // demo purposes only
    const progress = { style: '', color: color, value: value.value };
    const card = { style: '', bgColor: '' };

    if (variant === 'inverse') {
      progress.style = 'progress-white';
      progress.color = '';
      card.style = 'text-white';
      card.bgColor = 'bg-' + color;
    }

    const classes = mapToCssModules(classNames(className, card.style, card.bgColor), cssModule);
    progress.style = classNames('progress-xs my-3', progress.style);

    return (
      <Card className={classes} {...attributes}>
        <CardBody>
          <Row>
            <Col xs={2}>
              <img className="img-fluid" style={{minWidth: '40px'}} src={value.icon === 'travel' ?world : car} alt="World" />
            </Col>
            <Col xs={10}>
              <div className="h4 m-0">{value.targetName}</div>
              <div>{value.startDate}</div>
            </Col>
          </Row>
          
                    
          <Progress className={progress.style} color={value.color} value={progress.value} />
          <Row>
            <Col xs={5}>
              <div>Đã tiết kiệm: {value.saving}</div>
            </Col>
            <Col xs={5}>
              <div>Mục tiêu: {value.target}</div>
            </Col>
          </Row>
          
          <div>{children}</div>
          

        </CardBody>
      </Card>
    );
  }
}

Widget01.propTypes = propTypes;

export default Widget01;

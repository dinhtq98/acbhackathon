import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fireworks from '../../assets/img/fireworks.png';
import classNames from 'classnames';
import { mapToCssModules } from 'reactstrap/lib/utils';
import world from '../../assets/img/world.png';

import { Badge,Progress,Container,Button, Card, CardBody, CardHeader, Col, Row, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
const propTypes = {
  header: PropTypes.string,
  mainText: PropTypes.string,
  smallText: PropTypes.string,
  color: PropTypes.string,
  value: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  cssModule: PropTypes.object,
  variant: PropTypes.string,
};

const defaultProps = {
  targetName: '89.9%',
  mainText: 'Lorem ipsum...',
  smallText: 'Lorem ipsum dolor sit amet enim.',
  // color: '',
  value: '25',
  variant: '',
  saving: '15.000.000',
  target: '20.000.000',
};

class Widget05 extends Component {
  constructor(props) {
    super(props);
    this.toggleSuccess = this.toggleSuccess.bind(this);
    this.state = {
      isOpen: false,
      pause: false,
    };
  }
  pauseAction=()=>{
      this.setState({pause: true})   
  }
  continueAction=()=>{
      this.setState({pause: false})  
  }
  toggleSuccess() {
    this.setState({
      success: !this.state.success,
    });
  }
  render() {
    const { className, cssModule, header, mainText, smallText, color, value, children, variant, ...attributes } = this.props;
  
    // demo purposes only
    const progress = { style: '', color: color, value: value.value };
    const card = { style: '', bgColor: '' };

    if (variant === 'inverse') {
      progress.style = 'progress-white';
      progress.color = '';
      card.style = 'text-white';
      card.bgColor = 'bg-' + color;
    }

    const classes = mapToCssModules(classNames(className, card.style, card.bgColor), cssModule);
    progress.style = classNames('progress-xs my-3', progress.style);


    return (
      <Card className={classes} {...attributes}>
        <CardBody>
          <Row>
            <Col xs={2}>
              <img className="img-fluid" style={{minWidth: '40px'}} src={world} alt="World" />
            </Col>
            <Col xs={10}>
              <div className="h4 m-0">{value.targetName}</div>
              <div>{value.startDate}</div>
            </Col>
          </Row>
          
                    
          <Progress className={progress.style} color={progress.color} value={progress.value} />
          <Row style={{marginBottom:'10px'}}>
            <Col>
              <Badge className="mr-1" color="primary">{value.status}</Badge>
            </Col>
          </Row>
          <Row style={{marginBottom:'10px'}}>
            <Col xs={5}>
              <div>Đã tiết kiệm: {value.saving}</div>
            </Col>
            <Col xs={5}>
              <div>Mục tiêu: {value.target}</div>
            </Col>
          </Row >
          <Row style={{marginBottom:'30px'}}>
            <Col>
                <div>Số tiền để đạt mục tiêu: <b>{value.moneyPerMonth}</b>/tháng </div>
                
            </Col>
          </Row>
          <Row style={{textAlign:'center', marginBottom:'10px'}}>
          <Col>
            <Button color="primary"  block>Cập nhật</Button>
          </Col>
        </Row>
        <Row style={{textAlign:'center', marginBottom:'10px'}}>
          <Col>
            <Button color="success"  block onClick={this.toggleSuccess}>Hoàn thành</Button>
            <Modal isOpen={this.state.success} toggle={this.toggleSuccess}
                   className={'modal-success modal-dialog-centered ' + this.props.className}>
              <ModalHeader toggle={this.toggleSuccess}>Thông báo</ModalHeader>
              <ModalBody style={{alignItems:'center'}}>
                <Row>
                    <Col>
                        <b>CHÚC MỪNG BẠN ĐÃ HOÀN THÀNH MỤC TIÊU</b>
                    </Col>
                </Row>
                <Row style={{textAlign:'center'}}>
                  <Col >
                      <img className="img-fluid"  src={fireworks} alt="Fireworks" />
                  </Col>
                </Row>
                
              </ModalBody>
              <ModalFooter>
                <Button color="success" onClick={this.toggleSuccess}>OK</Button>
                
              </ModalFooter>
            </Modal>
          </Col>
        </Row>
        <Row style={{textAlign:'center', marginBottom:'10px'}}>
          <Col>
          {this.state.pause === false ?
            <Button color="warning"  block onClick={this.pauseAction}>Tạm dừng</Button> : 
            <Button color="warning"  block onClick={this.continueAction}>Tiếp tục</Button>
          }
          </Col>
        </Row>
          
          <div>{children}</div>
        </CardBody>
      </Card>
    );
  }
}

Widget05.propTypes = propTypes;

export default Widget05;

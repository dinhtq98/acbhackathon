import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
  Dropdown,
  ButtonDropdown, 
} from 'reactstrap';

class Targetform extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            amountTarget: 500000, 
            amountDone: 200000,
            goal: "",
            duration: "",
            des: "",
            name: ""
            }
    }
    handleChange = e => {
      this.setState({[e.target.name]: e.target.value})
    }
    // handleChangeTarget = (event, value, maskedValue) => {
    //     this.setState({ amountTarget: value })
    // }
    // handleChangeDone = (event, value, maskedValue) => {
    //     this.setState({ amountDone: value })
    // }
    // changeValue(e) {
    //     this.setState({dropDownValue: e.currentTarget.textContent})
    // }
    handleChangeTarget = event => {
        this.setState({ amountTarget: event.target.value });
    }
    handleChangeDone = event => {
        this.setState({ amountDone: event.target.value });
    }
    handleSubmit = event => {
        event.preventDefault();
        this.props.history.push('/muctieu');
    }
  render() {
    // const { amountTarget, amountDone } = this.state
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <strong>Thông tin mục tiêu</strong>
                {/* <small> Form</small> */}
              </CardHeader>
              <CardBody>
              <form onSubmit={this.handleSubmit}>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Tên mục tiêu</Label>
                      <Input type="text" name="name" placeholder="Ví dụ: Kế hoạch mua nhà" onChange={this.handleChange} required />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="goal">Mục đích</Label>  
                        <FormGroup>
                        <Input type="select" name="goal" id="goal">
                            <option value="1">Mua nhà</option>
                            <option value="2">Mua xe</option>
                            <option value="3">Du lịch</option>
                            <option value="4">Giáo dục</option>
                            <option value="5">Shopping</option>
                            <option value="6">Khác</option>
                        </Input>
                        </FormGroup>

                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label>Số tiền mục tiêu</Label>
                      <InputGroup>
                        <Input placeholder="" min={100000} max={10000000} type="number" step="100000" value={this.state.amountTarget} onChange={this.handleChange} name="amountTarget" required/>
                        {/* <InputGroupAddon addonType="append">.00</InputGroupAddon> */}
                        <InputGroupAddon addonType="prepend">VND</InputGroupAddon>
                      </InputGroup>
                      {/* <CurrencyInput value={this.state.amount} onChangeEvent={this.handleChange}/> */}
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label>Số tiền nạp vào</Label>
                      <InputGroup>
                        <Input placeholder="" min={100000} max={10000000} type="number" step="100000" value={this.state.amountDone} onChange={this.handleChange} name="amountDone" required/>
                        {/* <InputGroupAddon addonType="append">.00</InputGroupAddon> */}
                        <InputGroupAddon addonType="prepend">VND</InputGroupAddon>
                      </InputGroup>
                      {/* <CurrencyInput value={this.state.amount} onChangeEvent={this.handleChange}/> */}
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label>Bạn muốn hoàn thành mục tiêu này trong bao nhiêu tháng?</Label>
                      <InputGroup>
                        <Input placeholder="" min={1} type="number" name= "duration" onChange={this.handleChange} required/>
                        <InputGroupAddon addonType="prepend">tháng</InputGroupAddon>

                        {/* <InputGroupAddon addonType="append">.00</InputGroupAddon> */}
                      </InputGroup>
                      {/* <CurrencyInput value={this.state.amount} onChangeEvent={this.handleChange}/> */}
                    </FormGroup>
                  </Col>
                </Row>
                
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Mô tả</Label>  
                      <Input type="textarea" id="description" name="des" onChange={this.handleChange} />

                    </FormGroup>
                  </Col>
                </Row>
                
                <div className="form-actions">
                        <Button type="submit" color="primary">
                          <a href="http://localhost:3000/#/muctieu" style={{color: 'white', textDecoration: 'none'}}>Hoàn tất</a>
                        </Button>
                
                        <Button color="secondary">Để sau</Button>
                    
                </div>
                
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
      </div>
    );
  }
}

export default Targetform;

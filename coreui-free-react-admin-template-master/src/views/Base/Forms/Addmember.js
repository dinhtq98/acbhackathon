import React, { Component } from 'react';
// import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
  FormInput,
} from 'reactstrap';

import Targetform from './Targetform';

class Addmember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            owner: "1231231231",
            group_name: '',
            users: [],
            nextComponent: false,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      
      addClick(){
        this.setState(prevState => ({ 
            users: [...prevState.users, { account_number: "" }]
        }));
      }
      
      createUI(){
         return this.state.users.map((el, i) => (
           <div key={i}>
               <Row style={{paddingBottom: "20px"}}>
                   {/* <Col xs="4">
              <Input placeholder="Họ và tên" name="fullName" value={el.fullName ||''} onChange={this.handleChange.bind(this, i)} />
              </Col> */}
              <Col xs="8">
              <Input placeholder="Số tài khoản" name="account_number" type="number" value={el.account_number ||''} onChange={this.handleChange.bind(this, i)} required/>
              </Col>
              <Col xs="2" style={{textAlign: "center"}}>
              <Button color="danger" onClick={this.removeClick.bind(this, i)} >Xóa</Button>
              </Col>
              </Row>
           </div>          
         ))
      }
      
      handleChange(i, e) {
         const { name, value } = e.target;
         let users = [...this.state.users];
         users[i] = {...users[i], [name]: value};
         this.setState({ users });
      }

      updateGroupName = (e) => {
        if (e.target.name === 'group_name') {
          this.setState({group_name: e.target.value,});
        }
      }
      
      removeClick(i){
         let users = [...this.state.users];
         users.splice(i, 1);
         this.setState({ users });
      }
      
      handleSubmit(event) {
        event.preventDefault();
        const state = this.state
        if (state.owner && state.users) {
          const user_name = localStorage.getItem('username');
          fetch(`http://192.168.137.1:8080/create-group?username=${user_name}`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            group_name: state.group_name,
            members: state.users,
          }),
        })
        .then(res => res.text())
        .then(resJson => {
          console.log(resJson);
          this.setState({
            nextComponent: true,
          })
        })
        .catch(err => console.log(err));
          }
      }
    
      render() {
        return (
            this.state.nextComponent ? <Targetform /> : (
                <Card>
               <CardHeader>
                 <strong>Thêm thành viên</strong>
                 {/* <small> Form</small> */}
               </CardHeader>
               <CardBody>
          <Form onSubmit={this.handleSubmit}>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="groupname" style={{paddingBottom: "15px"}}><strong>Tên nhóm</strong></Label>
                      <Input name="group_name" type="text" id="groupname" placeholder="Ví dụ: Gia đình nhỏ" style={{paddingBottom: "20px"}} onChange={this.updateGroupName} required />
                      <Label htmlFor="listmember" style={{paddingTop: "15px"}} ><strong>Danh sách tài khoản</strong></Label>
                    </FormGroup>
                  </Col>
                </Row>
                <Row style={{paddingBottom: "20px"}}>
                   {/* <Col xs="4">
                    <Input placeholder="Họ và tên" name="fullName" value={el.fullName ||''} onChange={this.handleChange.bind(this, i)} />
                    </Col> */}
                    <Col xs="7">
                    <p> Số tài khoản của bạn: </p>

                    </Col>
                    <Col xs="5">
                    <b>{this.state.owner}</b>
                    </Col>
                    
                </Row>
              {this.createUI()}  
              <Row>
                  <Col>
                    <Button
                        type="button"
                        onClick={this.addClick.bind(this)}
                        className="small"
                        color="success"
                        >
                        Thêm thành viên
                    </Button>{'  '}
                    {/* <input type='button' value='add more' onClick={this.addClick.bind(this)}/> */}
                    <Button type="submit" color="warning"> Tiếp theo </Button>
                  </Col>
    
              </Row>     
          </Form>
          </CardBody>
          </Card>
            )
        );
      }
    // constructor() {
    //     super();
    //     this.state = {
    //       name: "",
    //       members: [],
    //     };
    //   }
    
    //   // ...
    
    // //   handlememberNameChange = idx => evt => {
    // //     const newmembers = this.state.members.map((member, sidx) => {
    // //       if (idx !== sidx) return member;
    // //       return { ...member, name: evt.target.value };
    // //     });
    
    // //     this.setState({ members: newmembers });
    // //   };
    
    // //   handleSubmit = evt => {
    // //     const { name, members } = this.state;
    // //     console.log(members)
    // //     alert(`Incorporated: ${name} with ${members.length} members`);
    // //   };
    
    // //   handleAddmember = () => {
    // //     this.setState({
    // //       members: this.state.members.concat([{ name: "" }])
    // //     });
    // //   };
    
    // //   handleRemovemember = idx => () => {
    // //     this.setState({
    // //       members: this.state.members.filter((s, sidx) => idx !== sidx)
    // //     });
    // //   };

    // handleChange = (event) => {
    //     this.setState({
    //         [event.target.name] : event.target.value,
    //     })
    // }

    // handleAdd = () => {
    //     const member = this.state.name;
    //     if (member) {
    //         this.state.members.push(member);
    //         this.setState({name: ''});
    //     }
    //     return;
    // }
    
    //   render() {
    //     return (
    //         <Card>
    //           <CardHeader>
    //             <strong>Thêm thành viên</strong>
    //             {/* <small> Form</small> */}
    //           </CardHeader>
    //           <CardBody>
    //       <form onSubmit={this.handleSubmit} >
    
    //         {this.state.members.map((member, idx) => (
    //           <div className="member">
    //             <Row style={{paddingBottom: "20px"}}>
    //               <Col xs="10">
    //             <Input
    //               type="text"
    //               placeholder={`Thành viên #${idx + 1}`}
    //               value={member.name}
    //               onChange={this.handleChange}
    //             />
    //             </Col>
    //             <Col xs="2" style={{textAlign: "center"}}>
    //             <Button
    //               type="button"
    //               onClick={this.handleRemovemember(idx)}
    //               className="small"
    //               color="danger"
    //             >
    //             Xóa
    //             </Button>
    //             </Col>
    //             </Row>
    //           </div>
    //         ))}
    //         <Button
    //           type="button"
    //           onClick={this.handleAddmember}
    //           className="small"
    //           color="success"
    //         >
    //           Thêm thành viên
    //         </Button>
    //         <Button>Incorporate</Button>
    //       </form>
    //       </CardBody>
    //       </Card>
    //     );
    // }
}

export default Addmember;

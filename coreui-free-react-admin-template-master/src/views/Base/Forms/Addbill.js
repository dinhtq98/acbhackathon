import React, { Component } from 'react';
import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
  Dropdown,
  ButtonDropdown, 
} from 'reactstrap';

class BillForm extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            type: "",
            amount: "",
            amountNeed: "",
            date: "",
            id_customer: "",
            acc_num: "",
            des: ""
            }
    }
    handleChange = (e) => {
      this.setState({[e.target.name]: e.target.value})
    }
    // handleChangeAmount = (e) => {
    //   this.setState({amount: e.target.value,})
    // }
    // handleChangeAmountNeed = (e) => {
    //   this.setState({amountNeed: e.target.value,})
    // }
    // handleChangeAmount = (event, value, maskedValue) => {
    //     this.setState({ amount: value })
    // }
    // handleChangeDone = (event, value, maskedValue) => {
    //     this.setState({ amountDone: value })
    // }
    // changeValue(e) {
    //     this.setState({dropDownValue: e.currentTarget.textContent})
    // }
  render() {
    // const { amountTarget, amountDone } = this.state
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="12">
            <Card>
              <CardHeader>
                <strong>Thông tin kế hoạch</strong>
                {/* <small> Form</small> */}
              </CardHeader>
              <CardBody>
              <form onSubmit={this.handleSubmit}>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="type"><strong>Loại hóa đơn</strong></Label>  
                        <FormGroup>
                        <Input type="select" name="type" id="type" onChange={this.handleChange} > 
                            <option value="1">Điện</option>
                            <option value="2">Nước</option>
                            <option value="3">Internet</option>
                            <option value="4">Phí chung cư</option>
                            <option value="5">Vay tiêu dùng</option>
                            <option value="6">Tiền mặt</option>
                            <option value="7">Chuyển khoản</option>
                            <option value="8">Khác...</option>
                        </Input>
                        </FormGroup>

                    </FormGroup>
                  </Col>
                </Row>
                
                {this.state.type == "6" || this.state.type == "7" || this.state.type == "8" ? (
                  <div>
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label><strong>Số tiền cần thanh toán</strong></Label>
                        <InputGroup>
                          <Input placeholder="" min={100000} max={10000000} type="number" step="100000" value={this.state.amountNeed} name="amountNeed" onChange={this.handleChange} required/>
                          {/* <InputGroupAddon addonType="append">.00</InputGroupAddon> */}
                          <InputGroupAddon addonType="prepend">VND</InputGroupAddon>
                        </InputGroup>
                        {/* <CurrencyInput value={this.state.amount} onChange={this.handleChange}/> */}
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label><strong>Ngày thanh toán</strong></Label>
                        <Input type="date" name="date" onChange={this.handleChange} required></Input>
                        {/* <CurrencyInput value={this.state.amount} onChange={this.handleChange}/> */}
                      </FormGroup>
                    </Col>
                  </Row>
                  </div>
                ):(
                  <div>
                    <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label><strong>Số tiền đã thanh toán tháng trước</strong></Label>
                        <InputGroup>
                          <Input placeholder="" min={100000} max={10000000} type="number" step="100000" value={this.state.amount} onChange={this.handleChange} name="amount" required/>
                          {/* <InputGroupAddon addonType="append">.00</InputGroupAddon> */}
                          <InputGroupAddon addonType="prepend">VND</InputGroupAddon>
                        </InputGroup>
                        {/* <CurrencyInput value={this.state.amount} onChange={this.handleChange}/> */}
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label><strong>Nhập mã số khách hàng</strong></Label>
                        <Input type="text" name="id_customer" onChange={this.handleChange} required></Input>
                        {/* <CurrencyInput value={this.state.amount} onChange={this.handleChange}/> */}
                      </FormGroup>
                    </Col>
                  </Row>
                </div>
                )}
                {this.state.type == "7"?(
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label><strong>Số tài khoản nhận</strong></Label>
                        <Input type="number" name="acc_num" onChange={this.handleChange} required></Input>
                        {/* <CurrencyInput value={this.state.amount} onChange={this.handleChange}/> */}
                      </FormGroup>
                    </Col>
                  </Row>
                  ):null}
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name"><strong>Mô tả</strong></Label>  
                      <Input type="textarea" id="description" name="des" onChange={this.handleChange} />

                    </FormGroup>
                  </Col>
                </Row>
                
                
                <div className="form-actions">
                        <Button type="submit" color="primary">
                          <a href="http://localhost:3000/#/plan" style={{color: 'white', textDecoration: 'none'}}>Kiểm tra</a>
                        </Button>{' '}
                
                        <Button color="secondary">Hủy</Button>
                    
                </div>
                
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
      </div>
    );
  }
}

export default BillForm;

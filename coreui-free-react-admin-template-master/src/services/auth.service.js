import API_URL from '../constants/API';
import axios from 'axios';

function getToken(endpoint, data) {
	console.log(data);
    return axios.post(API_URL+endpoint, JSON.stringify({
        username: data.username,
        password: data.password
    }));
}

export default {
	getToken
};

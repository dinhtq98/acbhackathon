const notification = [
    {
        userID : "BSQUOCHUY",
        content: "",
        time: "12/10/2019 13:58:23"
    },

    {
        userID : "BSQUOCHUY",
        content: "",
        time: "12/10/2019 16:35:11"
    },

    {
        userID : "HANGNGUYEN5887",
        content: "",
        time: "12/10/2019 10:26:55"
    },

    {
        userID : "NGUYEN_TIN",
        content: "",
        time: "12/10/2019 19:44:28"
    },

    {
        userID : "NGUYEN_TIN",
        content: "",
        time: "12/10/2019 23:22:47"
    }
];

const saving_goal = [
    {
        "target": "Du lich",
        "expected_money": 1500000,
        "time_in_month": 15,
        "description": "Da lat",
        "remaining_money": 100000
    },

    {
        "target": "Tien dien",
        "expected_money": 500000,
        "time_in_month": 10,
        "description": "",
        "remaining_money": 0
    },

    {
        "target": "Tien nha",
        "expected_money": 5000000,
        "time_in_month": 20,
        "description": "Nha",
        "remaining_money": 500000
    }
];

export default {
    notification,
    saving_goal
};